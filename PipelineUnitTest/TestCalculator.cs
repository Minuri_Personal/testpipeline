﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestPipeline;

namespace PipelineUnitTest
{
    [TestClass]
    public class TestCalculator
    {
        Calculator calculatorModel;

        [TestInitialize]
        public void SetUp()
        {
            calculatorModel = new Calculator();
        } 


        [TestMethod]
        public void TestSum()
        {
            // Act
            var result = calculatorModel.Sum(1, 2);

            // Assert
            Assert.AreEqual(3, result);
        }
    }
}
