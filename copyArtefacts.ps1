
$buildId = $env:CI_JOB_ID % 65535
$scanArtefactDirectory = "C:\GitLab-Runner\MinurisBuilds\BuildTempData"
$releaseArtefactDirectory = "C:\GitLab-Runner\MinurisBuilds\$env:CI_COMMIT_REF_NAME\$buildId"

if (!(Test-Path -path $scanArtefactDirectory))
{
	New-Item -path $scanArtefactDirectory -type "directory"
	Write-Host "Scan Artefact directory $scanArtefactDirectory created"
}

if (!(Test-Path -path $releaseArtefactDirectory))
{
	New-Item -path $releaseArtefactDirectory -type "directory"
	Write-Host "Release Artefact directory $releaseArtefactDirectory created"
}

if (!(Test-Path -path $scanArtefactDirectory\UnitTestData))
{
	New-Item -path $scanArtefactDirectory\UnitTestData -type "directory"
	Write-Host "Unit Test Artefact directory "$scanArtefactDirectory\UnitTestData" created"
}

Copy files to scan artefacts directory
try{
Copy-Item TestPipeline\bin\Release\*.exe -Destination "$scanArtefactDirectory\TestPipeline.zip"
}
catch{
	Write-Host $_
	Write-Host "Exception of copy artefacts"
}

# Copy files to release artefacts directory
Copy-Item TestPipeline\bin\Release\*.exe -Destination "$releaseArtefactDirectory"

# Copy files to unit test artefacts directory
copy-Item PipelineUnitTest\bin\Release $scanArtefactDirectory\UnitTestData\PipelineUnitTest\bin\Release -Recurse