
msbuild -t:restore

try
{
	msbuild /p:Configuration=Release
	if($LastExitCode.Equals(1))
	{
		throw;
	}
}
catch
{
	Write-Host "Build failed!"
	exit 1
}