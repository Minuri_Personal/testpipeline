
$testResultsFolder = "TestResults"
# $resultsFile = "testresults.trx"
$artefactsDirectory = "C:\GitLab-Runner\MinurisBuilds\BuildTempData\UnitTestData"

try{
    if(Test-Path -path $testResultsFolder){
        rmdir $testResultsFolder -recurse
        Write-Host "error on deleting unit test history"
    }
}
catch{
    Write-Host $_
    exit 1
}

try{
    Write-Host "execute tests"
    $testFiles = (Get-ChildItem -Path $artefactsDirectory -include ('*Tests.dll', '*Test.dll') -recurse).FullName
    # & vstest.console $testFiles /logger:trx[;LogFileName=<Defaults to unique file name>]
    $results = vstest.console $testFiles
}
catch{
    Write-Host $_
    exit 1
}

# Parse test results
try{
    if (!(Test-Path -path $testResultsFolder\$resultsFile)){
        Write-Host "No unit tests executed"
        exit 1
    }
    # $results = [xml](Get-Content $testResultsFolder\$resultsFile)

    # Write-Host Passed: $results.TestRun.ResultsSummary.Counters.passed
    # Write-Host Failed: $results.TestRun.ResultsSummary.Counters.failed
    Write-Host "write results"
    Write-Host $results
}
catch{
    Write-Host Parsing test results error
    Write-Host $_
    exit 1
}

